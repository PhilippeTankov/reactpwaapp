/* Créer un hook qui permet de fetch des données d'une API */

import { useState, useEffect } from 'react';
import axios from 'axios';

const UseAPI = () => {
    const [films, setFilms] = useState([]);


    // Use Effect
    useEffect(() => {
        axios.get('https://ghibliapi.herokuapp.com/films')
            .then(response => {
                console.log(response)
                setFilms(response.data)
            })
            .catch(error => {
                console.log(error)
            })
    }, [])
    return (
        <div>
            <ul>
                {films.map(film => (
                    <li key={film.id}>{film.title}</li>
                ))}
            </ul>
        </div>
    )
};

export default UseAPI;