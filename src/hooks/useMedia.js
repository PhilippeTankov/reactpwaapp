import {useState, useEffect} from 'react';

const DEVICES = {
    mobile: 200,
    tablet: 900
};


const useMedia = () => {
    const [device, setDevice] = useState('mobile');

    const updateDevice = () => {
        console.log(window.innerWidth);
        let deviceSize = 'desktop';
        if(window.innerWidth < DEVICES.mobile) deviceSize = 'mobile';
        else if(window.innerWidth < DEVICES.tablet) deviceSize = 'tablet';

        setDevice(deviceSize);
    };

    useEffect(() => {
        window.addEventListener('resize', updateDevice);
    }, []);

    return device;
};

export default useMedia;