import React, { useState, useMemo } from "react";
const Compo = () => {
    const [age, setAge] = useState(0);
    const [name, setName] = useState("");
    
    const uppercasedName = useMemo(() => toUpperCase(name), [name]);
    return (
        <div>
            <div>Age: {age}</div>
            <div>Nom: {uppercasedName}</div>
            <div>
                <input
                    value={age}
                    onChange={(e) => setAge(e.target.value)}
                    name="age"
                />
                <input
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                    name="name"
                />
            </div>
        </div>
    );
};
const toUpperCase = (string) => {
    let i = 0;
    while (i < 1000000000) i++;
    return string.toUpperCase();
};
export default Compo;