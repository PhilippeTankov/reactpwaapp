import React from 'react';
import logo from './logo.svg';
import './App.css';
import Compo from './Compo';
import useMedia from './hooks/useMedia';
import UseAPI from './hooks/UseAPI';

function App() {

  /* const device = useMedia('device');
  console.log({ device });

  if (device === 'mobile') return <div>vous êtes sur un téléphone</div>;
  else if (device === 'tablet') return <div>vous êtes sur une tablette</div>;
  return <div>vous êtes sur un desktop</div>; */
  return (
    <div className="App">
      {/* Test pour un hook qui nous montre notre taille d'écran */}
      <Compo/>

      {/* TP 1 - Noté */}
      <UseAPI />
    </div>
  );
}

export default App;
